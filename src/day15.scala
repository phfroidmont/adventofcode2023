package aoc
package day15

val dayNumber = "15"

@main def part1: Unit =
  println(part1(loadInput(dayNumber)))

@main def part2: Unit =
  println(part2(loadInput(dayNumber)))

def part1(input: String): String =
  input
    .split(',')
    .map(_.foldLeft(0)((acc, c) => ((acc + c) * 17) % 256))
    .sum
    .toString

def part2(input: String): String =
  val hash: String => Int = _.foldLeft(0)((acc, c) => ((acc + c) * 17) % 256)
  val steps = input
    .split(',')
    .map {
      case s"$label=$value" => (label, hash(label), Some(value.toInt))
      case s"$label-"       => (label, hash(label), None)
    }
  val boxes = Map.empty[Int, List[(String, Int)]]
  steps
    .foldLeft(boxes) {
      case (boxes, (label, number, Some(value))) =>
        boxes.updatedWith(number) {
          case Some(lenses) =>
            val i = lenses.indexWhere(_._1 == label)
            if i >= 0 then Some(lenses.updated(i, (label, value)))
            else Some(lenses.appended((label, value)))
          case None => Some(List((label, value)))
        }
      case (boxes, (label, number, None)) =>
        boxes.updatedWith(number)(_.map(_.filterNot(_._1 == label)))

    }
    .flatMap((boxNumber, lenses) =>
      lenses.zipWithIndex.map((s, i) => s._2 * (i + 1) * (boxNumber + 1))
    )
    .sum
    .toString
