package aoc
package day19

import scala.annotation.tailrec
import scala.collection.immutable.NumericRange

val dayNumber = "19"

@main def part1: Unit =
  println(part1(loadInput(dayNumber)))

@main def part2: Unit =
  println(part2(loadInput(dayNumber)))

enum Category:
  case x, m, a, s
type Part = Map[Category, Int]

enum Rule:
  case Cond(
      category: Category,
      less: Boolean,
      value: Int,
      action: String
  )
  case Default(action: String)

type Workflow = List[Rule]
object Workflow:
  def parseAll(str: String) =
    str
      .split('\n')
      .map { case s"$name{$rules}" =>
        name -> rules
          .split(',')
          .map {
            case s"$cat<$value:$res" =>
              Rule.Cond(Category.valueOf(cat), true, value.toInt, res)
            case s"$cat>$value:$res" =>
              Rule.Cond(Category.valueOf(cat), false, value.toInt, res)
            case default => Rule.Default(default)
          }
          .toList
      }
      .toMap

def part1(input: String): String =

  val Array(workflowsStr, partsStr) = input.split("\n\n")

  val parts = partsStr
    .split('\n')
    .map[Part] { case s"{x=$x,m=$m,a=$a,s=$s}" =>
      Map(
        Category.x -> x.toInt,
        Category.m -> m.toInt,
        Category.a -> a.toInt,
        Category.s -> s.toInt
      )
    }
    .toList

  val workflows = Workflow.parseAll(workflowsStr)

  @tailrec def isAccepted(nextAction: String, part: Part): Boolean =
    nextAction match
      case "A" => true
      case "R" => false
      case name =>
        isAccepted(
          workflows(name).collectFirst {
            case Rule.Cond(cat, less, v, action)
                if less && part(cat) < v || !less && part(cat) > v =>
              action
            case Rule.Default(action) => action
          }.get,
          part
        )

  parts
    .filter(isAccepted("in", _))
    .map(_.values.sum)
    .sum
    .toString

def part2(input: String): String =
  val workflowsStr = input.split("\n\n").head

  val workflows = Workflow.parseAll(workflowsStr)

  type PartRange = Map[Category, NumericRange[Long]]

  def count(nextAction: String, part: PartRange): Long =
    nextAction match
      case "A" => part.values.map(_.length.toLong).product
      case "R" => 0
      case name =>
        var nextPart = part
        workflows(name).map {
          case Rule.Cond(cat, less, v, action) if less =>
            val (low, high) =
              nextPart(cat).splitAt((v - nextPart(cat).head).toInt)
            val lowCount = count(action, nextPart.updated(cat, low))
            nextPart = nextPart.updated(cat, high)
            lowCount
          case Rule.Cond(cat, less, v, action) =>
            val (low, high) =
              nextPart(cat).splitAt((v - nextPart(cat).head + 1).toInt)
            val highCount = count(action, nextPart.updated(cat, high))
            nextPart = nextPart.updated(cat, low)
            highCount
          case Rule.Default(action) => count(action, nextPart)
        }.sum

  count("in", Category.values.map(cat => cat -> (1L to 4000L)).toMap).toString
