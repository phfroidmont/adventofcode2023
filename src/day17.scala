package aoc
package day17

import scala.collection.mutable

val dayNumber = "17"

@main def part1: Unit =
  println(part1(loadInput(dayNumber)))

@main def part2: Unit =
  println(part2(loadInput(dayNumber)))

enum D:
  case Up, Down, Left, Right
object D:
  val opposite: D => D =
    case Up    => Down
    case Down  => Up
    case Right => Left
    case Left  => Right

import D.*

def leastHeatLoss(grid: Array[Array[Int]], min: Int, max: Int) =

  final case class Crucible(
      x: Int,
      y: Int,
      d: D,
      movesCount: Int,
      heatLoss: Int
  ):
    def nextBlocks =
      if movesCount < min then Array(go(d).updateMovesCount(d))
      else if movesCount < max then
        D.values.filterNot(_ == D.opposite(d)).map(go(_).updateMovesCount(d))
      else
        D.values
          .filterNot(nextD => nextD == d || nextD == D.opposite(d))
          .map(go(_).updateMovesCount(d))
    def updateMovesCount(prevD: D) =
      copy(movesCount = if prevD == d then movesCount + 1 else 1)
    val go: D => Crucible =
      case Up    => copy(y = y - 1, d = Up)
      case Down  => copy(y = y + 1, d = Down)
      case Left  => copy(x = x - 1, d = Left)
      case Right => copy(x = x + 1, d = Right)
    def isOutOfBounds(grid: Array[Array[Int]]) =
      x < 0 || x >= grid.head.length || y < 0 || y >= grid.length
    def looseHeat(grid: Array[Array[Int]]) =
      copy(heatLoss =
        heatLoss + (if isOutOfBounds(grid) then 0 else grid(y)(x))
      )

  given Ordering[Crucible] = Ordering.by(c => -c.heatLoss)
  val height = grid.length
  val width = grid.head.length
  val seen = mutable.Map((0, 0, Up, 0) -> 0)
  val queue = mutable.PriorityQueue(
    Crucible(1, 0, Right, 1, grid(0)(1)),
    Crucible(0, 1, Down, 1, grid(1)(0))
  )

  def isTheEnd(c: Crucible) = c.x == width - 1 && c.y == height - 1
  def hasSeenBetter(c: Crucible) =
    seen
      .get((c.x, c.y, c.d, c.movesCount))
      .map(_ <= c.heatLoss)
      .getOrElse(false)

  while (!isTheEnd(queue.head))
    val c = queue.dequeue
    if !c.isOutOfBounds(grid) && !hasSeenBetter(c) then
      seen.update((c.x, c.y, c.d, c.movesCount), c.heatLoss)
      c.nextBlocks
        .map(_.looseHeat(grid))
        .foreach(queue.enqueue(_))
  queue.head.heatLoss

def part1(input: String): String =
  val grid = input.split('\n').map(_.map(_.asDigit).toArray)
  leastHeatLoss(grid, 1, 3).toString

def part2(input: String): String =
  val grid = input.split('\n').map(_.map(_.asDigit).toArray)
  leastHeatLoss(grid, 4, 10).toString
