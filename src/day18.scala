package aoc
package day18

val dayNumber = "18"

@main def part1: Unit =
  println(part1(loadInput(dayNumber)))

@main def part2: Unit =
  println(part2(loadInput(dayNumber)))

final case class Point(x: Long, y: Long)

def computeArea(points: List[Point]): Long =
  // https://en.wikipedia.org/wiki/Shoelace_formula
  val lagoonArea =
    points
      .zip(points.tail)
      .map((a, b) => a.x * b.y - b.x * a.y)
      .sum
      .abs / 2
  val trenchesArea =
    points
      .zip(points.tail)
      .map((a, b) => (b.x - a.x + b.y - a.y).abs)
      .sum

  lagoonArea + trenchesArea / 2 + 1

def part1(input: String): String =
  val points = input
    .split('\n')
    .foldLeft(List(Point(0, 0))) { case (acc, s"$d $m $_") =>
      d match
        case "R" => Point(acc.head.x + m.toLong, acc.head.y) :: acc
        case "L" => Point(acc.head.x - m.toLong, acc.head.y) :: acc
        case "U" => Point(acc.head.x, acc.head.y - m.toLong) :: acc
        case "D" => Point(acc.head.x, acc.head.y + m.toLong) :: acc
    }
  computeArea(points).toString

def part2(input: String): String =
  val points = input
    .split('\n')
    .foldLeft(List(Point(0, 0))) { case (acc, s"$_ $_ (#$hex)") =>
      val m = Integer.parseInt(hex.dropRight(1), 16)
      hex.last match
        case '0' => Point(acc.head.x + m, acc.head.y) :: acc
        case '2' => Point(acc.head.x - m, acc.head.y) :: acc
        case '3' => Point(acc.head.x, acc.head.y - m) :: acc
        case '1' => Point(acc.head.x, acc.head.y + m) :: acc
    }
  computeArea(points).toString
