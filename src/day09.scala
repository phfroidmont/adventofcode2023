package aoc
package day09

import scala.annotation.tailrec

val dayNumber = "09"

@main def part1: Unit =
  println(part1(loadInput(dayNumber)))

@main def part2: Unit =
  println(part2(loadInput(dayNumber)))

def findNextValue(history: Array[Long]): Long =
  @tailrec def aux(history: Array[Long], acc: Long): Long =
    if history.forall(_ == 0) then acc
    else
      aux(
        history.sliding(2).map { case Array(l, r) => r - l }.toArray,
        acc + history.last
      )
  aux(history, 0)

def part1(input: String): String =
  val readings = input.split('\n').map(_.split(' ').map(_.toLong))
  readings.map(findNextValue).sum.toString

def part2(input: String): String =
  val readings = input.split('\n').map(_.split(' ').map(_.toLong))
  readings.map(_.reverse).map(findNextValue).sum.toString
