package aoc
package day10

import scala.annotation.tailrec

val dayNumber = "10"

@main def part1: Unit =
  println(part1(loadInput(dayNumber)))

@main def part2: Unit =
  println(part2(loadInput(dayNumber)))

def getConnected(grid: Array[Array[Char]], x: Int, y: Int) = grid(y)(x) match
  case '|' => List(x -> (y - 1), x -> (y + 1))
  case '-' => List(x - 1 -> y, x + 1 -> y)
  case 'L' => List(x -> (y - 1), x + 1 -> y)
  case 'J' => List(x -> (y - 1), x - 1 -> y)
  case '7' => List(x -> (y + 1), x - 1 -> y)
  case 'F' => List(x -> (y + 1), x + 1 -> y)
  case 'S' => List(x - 1 -> y, x + 1 -> y, x -> (y - 1), x -> (y + 1))
  case _   => List()

def prepareInput(input: String): Array[Array[Char]] =
  val lines = input.split('\n')
  val emptyLine = (0 until lines.head.length + 2).map(_ => '.').mkString
  lines
    .map(l => s".$l.")
    .prepended(emptyLine)
    .appended(emptyLine)
    .map(_.toCharArray)

@tailrec def findPath(
    grid: Array[Array[Char]],
    x: Int,
    y: Int,
    path: List[(Int, Int)]
): List[(Int, Int)] =
  val next = getConnected(grid, x, y)
    .filter(!path.contains(_))
    .find((aX, aY) => getConnected(grid, aX, aY).contains(x -> y))
  if next.isEmpty then path
  else findPath(grid, next.head._1, next.head._2, next.head :: path)

def part1(input: String): String =
  val grid = prepareInput(input)
  val sY = grid.indexWhere(_.contains('S'))
  val sX = grid(sY).indexOf('S')
  val loopLength = findPath(grid, sX, sY, List(sX -> sY)).length
  (loopLength / 2).toString

def part2(input: String): String =
  val grid = prepareInput(input)
  val sY = grid.indexWhere(_.contains('S'))
  val sX = grid(sY).indexOf('S')
  val path = findPath(grid, sX, sY, List(sX -> sY)).toSet

  def isInside(x: Int, y: Int) =
    val (up, down) = (0 until x)
      .map((_, y))
      .filter(path.contains)
      .foldLeft((0, 0)) { case ((u, d), (x, y)) =>
        grid(y)(x) match
          case '|' => (u + 1, d + 1)
          case 'L' => (u + 1, d)
          case 'J' => (u + 1, d)
          case '7' => (u, d + 1)
          case 'F' => (u, d + 1)
          case 'S' => (u + 1, d + 1) // Specific to my input, shaped like '|'
          case _   => (u, d)
      }
    up % 2 == 1 && down % 2 == 1

  (for
    x <- 0 until grid.head.length
    y <- 0 until grid.length
    if !path.contains(x -> y) && isInside(x, y)
  yield 1).sum.toString
