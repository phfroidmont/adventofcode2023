package aoc
package day01

import scala.annotation.tailrec

val dayNumber = "01"

@main def part1: Unit =
  println(part1(loadInput(dayNumber)))

@main def part2: Unit =
  println(part2(loadInput(dayNumber)))

def part1(input: String): String =
  input
    .split("\n")
    .map(line =>
      line.find(_.isDigit).get.toString + line.findLast(_.isDigit).get.toString
    )
    .map(_.toInt)
    .sum
    .toString

def part2(input: String): String =
  val digitsList = (1 to 9).map(_.toString)
  val digitsMap =
    List("one", "two", "three", "four", "five", "six", "seven", "eight", "nine")
      .zip(digitsList)
      .prependedAll(digitsList.zip(digitsList))

  @tailrec def findFistDigit(line: String): String =
    digitsMap.find((key, _) => line.startsWith(key)) match
      case Some((_, digit)) => digit
      case None             => findFistDigit(line.substring(1))

  @tailrec def findLastDigit(line: String): String =
    digitsMap.find((key, _) => line.endsWith(key)) match
      case Some((_, digit)) => digit
      case None             => findLastDigit(line.substring(0, line.length - 1))

  input
    .split("\n")
    .map(line => findFistDigit(line) + findLastDigit(line))
    .map(_.toInt)
    .sum
    .toString
