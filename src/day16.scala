package aoc
package day16

import scala.collection.mutable

val dayNumber = "16"

@main def part1: Unit =
  println(part1(loadInput(dayNumber)))

@main def part2: Unit =
  println(part2(loadInput(dayNumber)))

enum D:
  case Up, Down, Left, Right
import D.*

final case class BeamSection(x: Int, y: Int, d: D):
  val go: D => BeamSection =
    case Up    => BeamSection(x, y - 1, Up)
    case Down  => BeamSection(x, y + 1, Down)
    case Left  => BeamSection(x - 1, y, Left)
    case Right => BeamSection(x + 1, y, Right)

def countEnergized(grid: Array[String], start: BeamSection): Int =
  val energized = mutable.Set.empty[BeamSection]

  def walk(s: BeamSection): Unit =
    if energized.contains(s) ||
      s.x < 0 || s.x >= grid.head.length ||
      s.y < 0 || s.y >= grid.length
    then ()
    else
      energized.add(s)
      (grid(s.y)(s.x), s.d) match
        case ('\\', Up)    => walk(s.go(Left))
        case ('\\', Down)  => walk(s.go(Right))
        case ('\\', Left)  => walk(s.go(Up))
        case ('\\', Right) => walk(s.go(Down))
        case ('/', Up)     => walk(s.go(Right))
        case ('/', Down)   => walk(s.go(Left))
        case ('/', Right)  => walk(s.go(Up))
        case ('/', Left)   => walk(s.go(Down))
        case ('|', Right) | ('|', Left) =>
          walk(s.go(Up))
          walk(s.go(Down))
        case ('-', Up) | ('-', Down) =>
          walk(s.go(Left))
          walk(s.go(Right))
        case (_, d) => walk(s.go(d))

  walk(start)
  energized.map(s => (s.x, s.y)).size

def part1(input: String): String =
  countEnergized(input.split('\n'), BeamSection(0, 0, Right)).toString

def part2(input: String): String =
  val grid = input.split('\n')
  val height = grid.length
  val width = grid.head.length
  val startPositions =
    grid.head.indices.flatMap(x =>
      List(BeamSection(x, 0, Down), BeamSection(x, height - 1, Up))
    ) ++
      grid.indices.flatMap(y =>
        List(BeamSection(0, y, Right), BeamSection(y, width - 1, Left))
      )
  startPositions.map(countEnergized(grid, _)).max.toString
