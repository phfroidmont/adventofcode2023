package aoc
package day13

val dayNumber = "13"

@main def part1: Unit =
  println(part1(loadInput(dayNumber)))

@main def part2: Unit =
  println(part2(loadInput(dayNumber)))

def part1(input: String): String =
  def findReflection(rows: List[String]): Option[Int] =
    (1 until rows.length).find(i =>
      val (up, down) = rows.splitAt(i)
      up.reverse.zip(down).forall(_ == _)
    )

  input
    .split("\n\n")
    .map(_.split('\n').toList)
    .flatMap(pattern =>
      findReflection(pattern)
        .map(_ * 100)
        .orElse(findReflection(pattern.transpose.map(_.mkString)))
    )
    .sum
    .toString

def part2(input: String): String =
  def findReflection(rows: List[String]): Option[Int] =
    (1 until rows.length).find(i =>
      val (up, down) = rows.splitAt(i)
      val linesToCompare = up.reverse.zip(down)
      val area = linesToCompare.length * linesToCompare.head._1.length
      val differencesCount = linesToCompare.map(_.zip(_).count(_ == _)).sum
      area - differencesCount == 1
    )

  input
    .split("\n\n")
    .map(_.split('\n').toList)
    .flatMap(pattern =>
      findReflection(pattern)
        .map(_ * 100)
        .orElse(findReflection(pattern.transpose.map(_.mkString)))
    )
    .sum
    .toString
