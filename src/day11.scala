package aoc
package day11

import Math.abs
import util.chaining.*

val dayNumber = "11"

@main def part1: Unit =
  println(part1(loadInput(dayNumber)))

@main def part2: Unit =
  println(part2(loadInput(dayNumber)))

def part1(input: String): String =
  def expandVertically(grid: Array[Array[Char]]) =
    grid.flatMap(l => if l.contains('#') then List(l) else List(l, l))

  def transpose(grid: Array[Array[Char]]): Array[Array[Char]] =
    Array.tabulate(grid.head.length)(x =>
      Array.tabulate(grid.length)(y => grid(y)(x))
    )

  val grid = input
    .split('\n')
    .map(_.toCharArray)
    .pipe(expandVertically)
    .pipe(transpose)
    .pipe(expandVertically)
    .tapEach(l => println(l.mkString))

  val galaxies =
    (for
      x <- grid.head.indices
      y <- grid.indices
      if grid(y)(x) == '#'
    yield (x, y))

  galaxies
    .combinations(2)
    .map { case Seq((xa, ya), (xb, yb)) => abs(xb - xa) + abs(yb - ya) }
    .sum
    .toString

def part2(input: String): String =
  val grid = input
    .split('\n')

  val expandedRows = grid.indices.filter(y => !grid(y).contains('#'))
  val expandedCols = grid.head.indices.filter(x => grid.forall(_(x) != '#'))

  val expansionMult: Long = 1_000_000 - 1

  val galaxies =
    (for
      x <- grid.head.indices
      y <- grid.indices
      if grid(y)(x) == '#'
    yield (x, y))

  galaxies
    .combinations(2)
    .map { case Seq((xa, ya), (xb, yb)) =>
      val colExpansions =
        expandedCols.count(x => xa < x && x < xb || xb < x && x < xa)
      val rowExpansions =
        expandedRows.count(y => ya < y && y < yb || yb < y && y < ya)
      abs(xb - xa) + abs(yb - ya) +
        colExpansions * expansionMult +
        rowExpansions * expansionMult
    }
    .sum
    .toString
