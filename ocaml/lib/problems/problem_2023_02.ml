let year = 2023
let day = 1

module Part_1 = struct
  type color = Red | Blue | Green
  type set = { count : int; color : color }
  type game = { number : int; sets : set list }

  let print_game { number ; _ } =
    Printf.sprintf "%i" number

  let run (input : string) : (string, string) result =
    let parse_color = function
      | "red" -> Red
      | "blue" -> Blue
      | "green" -> Green
      | _ -> assert false
    in
    let parse_set str =
      match String.split_on_char ',' str with
      | [ number; color ] ->
          { count = int_of_string number; color = parse_color color }
      | _ -> assert false
    in
    let parse_sets str = String.split_on_char ';' str |> List.map parse_set in
    let parse_game_number str =
      match String.trim str |> String.split_on_char ' ' with
      | [ _; number ] -> int_of_string number
      | _ -> assert false
    in
    let parse_line str =
      match String.split_on_char ':' str with
      | [ prefix; suffix ] ->
          { number = parse_game_number prefix; sets = parse_sets suffix }
      | _ -> assert false
    in
    let (games : game list) =
      input |> String.trim |> String.split_on_char '\n' |> List.map parse_line
    in
    Ok (List.hd games |> print_game)
end

module Part_2 = struct
  let run (input : string) : (string, string) result =
    Ok input
end
